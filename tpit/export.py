#!/usr/bin/python3

import geojson
import sqlite3

def dict_factory(cursor, row):
	d = {}
	for idx, col in enumerate(cursor.description):
		d[col[0]] = row[idx]
	return d

con = sqlite3.connect('tpit.db')
con.row_factory = dict_factory
projects = con.execute("""select *, min(sheet_date) AS first_seen, max(sheet_date) AS last_seen
FROM tpit
GROUP BY id
ORDER BY CASE WHEN sheet_status='Complete' THEN 0 WHEN sheet_status='Future' THEN 1 WHEN sheet_status='Cancelled' THEN 2 END, projected_in_service;
""")

county_projects = {}

for proj in projects:
	county = proj['county_start']
	if not county in county_projects:
		county_projects[county] = []
	county_projects[county].append(proj)

with open('tx_counties.geojson', 'r') as f:
	county_geojson = geojson.load(f)

for county in county_geojson.features:
	name = county.properties["name"]
	projects = county_projects.get(name, [])
	county.properties['projects'] = projects
	count = 0
	for p in projects:
		if p['sheet_status'] != 'Cancelled':
			count = count + 1
	county.properties['project_count'] = count

with open('tpit.geojson', 'w') as f:
	geojson.dump(county_geojson, f)
